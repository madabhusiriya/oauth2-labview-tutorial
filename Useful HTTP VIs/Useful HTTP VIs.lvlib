﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Unit Tests" Type="Folder">
		<Item Name="million-a-string.vi" Type="VI" URL="../unit tests/million-a-string.vi"/>
		<Item Name="SHA256.lvtest" Type="TestItem" URL="../unit tests/SHA256.lvtest">
			<Property Name="utf.test.bind" Type="Str">Useful HTTP VIs.lvlib:SHA256.vi</Property>
			<Property Name="utf.vector.test.bind" Type="Str">0E0713D4-D4C0-0E95-68DD-3DD5383FDD18</Property>
		</Item>
		<Item Name="User_Defined_Test_million_SHA256.lvtest" Type="TestItem" URL="../unit tests/User_Defined_Test_million_SHA256.lvtest">
			<Property Name="utf.test.bind" Type="Str">Useful HTTP VIs.lvlib:SHA256.vi</Property>
			<Property Name="utf.vector.test.bind" Type="Str">3EBA96EE-7D51-309C-EC6A-8E0A5AD32FE8</Property>
		</Item>
		<Item Name="User_Defined_Test_million_SHA256.vi" Type="VI" URL="../unit tests/User_Defined_Test_million_SHA256.vi"/>
		<Item Name="Util Convert to Base64url No Padding.lvtest" Type="TestItem" URL="../unit tests/Util Convert to Base64url No Padding.lvtest">
			<Property Name="utf.test.bind" Type="Str">Useful HTTP VIs.lvlib:Util Convert to Base64url No Padding.vi</Property>
			<Property Name="utf.vector.test.bind" Type="Str">B0B9D758-C9B7-C233-5C08-4315E5BD788F</Property>
		</Item>
	</Item>
	<Item Name="Parse HTTP Response Headers.vi" Type="VI" URL="../Parse HTTP Response Headers.vi"/>
	<Item Name="Parse URI.vi" Type="VI" URL="../Parse URI.vi"/>
	<Item Name="SHA256.vi" Type="VI" URL="../SHA256.vi"/>
	<Item Name="Simple HTTP Server.vi" Type="VI" URL="../Simple HTTP Server.vi"/>
	<Item Name="Util Convert to Base64url No Padding.vi" Type="VI" URL="../Util Convert to Base64url No Padding.vi"/>
</Library>
