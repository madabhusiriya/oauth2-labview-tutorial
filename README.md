# OAuth2 LabVIEW Tutorial

An example of how to do OAuth2 authentication with LabVIEW 2019 and later.

A detailed description can be found on the Stravaro Blog:

[OAuth2 and LabVIEW -- The Evolution of an Example](https://stravaro.com/2020/01/oauth2-and-labview/)